# Endpoints Configuration



```shell

Request Type: GET
Endpoint: /record
Status: 200
Response: “Hello, This is a SlimApp”
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Request Type: GET
Endpoint: /record/:id
Response status: 200 or 404
Response: “record :id” or “record Not Found”
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Request Type: POST, PUT, PATCH
Endpoint: /record/:id
Response status: 200 or 404
Response: “record :id, updated successfully” or “record Not Found”
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Request Type: DELETE
Endpoint: /record/:id
Response status: 200 or 404
Response: “record :id, deleted successfully” or “record Not Found”
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

## Installation

- All the `code` required to get started
- Images of what it should look like

### Clone

- Clone this repo to your local machine using `git clone https://gitlab.com/rutigliano/unit-testing-slim-app.git`

### Setup

- If you want to run this project on a MAC:

> update and install this packages first

* Homebrew 

```shell
$ xcode-select --install
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
* run this command to find out if brew is installed properly

```shell
$ brew doctor
```

```shell
$ brew update
```

> now install php  and composer packages

```shell
$ brew install php
$ brew install composer
$ composer global require squizlabs/php_codesniffer
```

> to run the project lets install all the dependencies
```shell   
$ composer install
```

> Then lets serve the app in the browser by running the command

```shell
$ ./serve public
```


