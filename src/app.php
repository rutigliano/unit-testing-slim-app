<?php

/**
 * App Index
 *
 * Main file for the App.
 *
 *  * PHP version 7
 *
 * @category   Application
 * @package    SlimApp/PhpUnit
 * @subpackage AppClasses
 * @author     Domenico Rutigliano <wdetcetera@gmail.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @version    GIT: 1.0.1
 * @link       ****
 * @since      1.0.0
 */

namespace Dom\SlimApp;

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

/**
 * This is an example of Class Docs
 *
 * @category   CRUDApps
 * @package    SlimApp
 * @author     Domenico Rutigliano <wdetcetera@gmail.com>
 * @copyright  1997-2019 GNU
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0.1
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 * @deprecated Class deprecated in Release 2.0.0
 **/

class App
{
    /**
     * Stores an instance of the Slim application.
     *
     * @var \Slim\App
     */
    private $_app;

    /**
     * The constructor
     *
     * @access     public
     * @static
     * @deprecated Method deprecated in Release 2.0.0
     **/
    public function __construct()
    {
        $_app = new \Slim\App;

        $_app->get(
            '/', function (Request $request, Response $response) {
                $response->getBody()->write("Hello, This is a SlimApp");
                return $response;
            }
        );

        $_app->group(
            '/record', function () {
                $recordIdValid = function ($id) {
                    return (int) $id && $id > 0 && $id <= 10;
                };

                $this->map(
                    ['GET'], '', function (Request $request, Response $response) {
                        return $response->withJson(['message' => 'Hello, Record from SlimApp']);
                    }
                );

                $this->get(
                    '/{id}', function (Request $request, Response $response, $args) use ($recordIdValid) {
                        if ($recordIdValid($args['id'])) {
                            return $response->withJson(['message' => "record " . $args['id']]);
                        }

                        return $response->withJson(['message' => 'record Not Found'], 404);
                    }
                );

                $this->map(
                    ['POST', 'PUT', 'PATCH'], '/{id}', function (Request $request, Response $response, $args) use ($recordIdValid) {
                        if ($recordIdValid($args['id'])) {
                            $message=[
                                'message' => "record " 
                                . $args['id'] 
                                . " updated successfully"];
                            return $response->withJson($message);
                        }

                        return $response->withJson(['message' => 'record Not Found'], 404);
                    }
                );

                $this->delete(
                    '/{id}', function (Request $request, Response $response, $args) use ($recordIdValid) {
                        if ($recordIdValid($args['id'])) {
                            $message = [
                                'message' => "record " 
                                . $args['id'] 
                                . " deleted successfully"];
                            return $response->withJson($message);
                        }

                        return $response->withJson(['message' => 'record Not Found'], 404);
                    }
                );
            }
        );

        $this->app = $_app;
    }

    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }
}
