<?php
/**
 * App Unit Tests
 *
 * Main file for the App Tests
 *
 *  * PHP version 7
 *
 * @category   UniteTests
 * @package    SlimApp/PhpUnit
 * @subpackage None
 * @author     Domenico Rutigliano <wdetcetera@gmail.com>
 * @license    https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @version    GIT: 1.0.1
 * @link       ****
 * @since      1.0.0
 */

use Dom\SlimApp\App;
use Slim\Http\Environment;
use Slim\Http\Request;

/**
 * This is an example of Class Docs
 *
 * @category   UnitTesting
 * @package    SlimApp
 * @author     Domenico Rutigliano <wdetcetera@gmail.com>
 * @copyright  1997-2019 GNU Opensource
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0.1
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 * @deprecated Class deprecated in Release 2.0.0
 **/

class RecordTest extends PHPUnit_Framework_TestCase
{
    protected $app;

    /**
     * Setup
     *
     * @access     public
     * @static
     * @deprecated Method deprecated in Release 2.0.0
     * @return     TestOutput
     **/
    public function setUp()
    {
        $this->app = (new App())->get();
    }
    /**
     * RegordGet Test
     *
     * @access     public
     * @static
     * @deprecated Method deprecated in Release 2.0.0
     * @return     TestOutput
     **/
    public function testRecordGet()
    {
        $env = Environment::mock(
            [
                'REQUEST_METHOD' => 'GET',
                'REQUEST_URI' => '/',
            ]
        );
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $this->assertSame((string) $response->getBody(), "Hello, This is a SlimApp");
    }

    /**
     * Regord Get by an ID which exist, aspecting a 200 response
     *
     * @access     public
     * @static
     * @deprecated Method deprecated in Release 2.0.0
     * @return     TestOutput
     **/
    public function testRecordGetbyExistingID()
    {
        $testRec = 9;
        $env = Environment::mock(
            [
                'REQUEST_METHOD' => 'GET',
                'REQUEST_URI' => '/record/'.$testRec,
            ]
        );
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $output='{"message":"record '.$testRec.'"}';
        $this->assertSame((string) $response->getBody(), $output);
    }

    /**
     * Regord Get by an ID which  does not exist, aspecting a 400 response
     *
     * @access     public
     * @static
     * @deprecated Method deprecated in Release 2.0.0
     * @return     TestOutput
     **/
    public function testRecordGetbyNotExistingID()
    {
        $testRec = 20;
        $env = Environment::mock(
            [
                'REQUEST_METHOD' => 'GET',
                'REQUEST_URI' => '/record/'.$testRec,
            ]
        );
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 404);
        $output='{"message":"record Not Found"}';
        $this->assertSame((string) $response->getBody(), $output);
    }

 
}
