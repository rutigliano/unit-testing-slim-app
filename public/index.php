<?php

require '../vendor/autoload.php';
// Run app
$app = (new Dom\SlimApp\App())->get();
$app->run();
